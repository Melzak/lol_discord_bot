import os
from random import randint
from typing import List, Tuple

import discord
from discord.ext import commands


class LOL(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="runes", help="Sends runes for selected champ")
    async def runes(self, ctx, *champ: str, **kwargs):
        """Checks if there is file like champion_name.png
        if file exists then sends file to discord

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param champ: Holds all parts of champion's name
        :type champ: Tuple[str]
        """
        if champ:
            champ_name = "".join(champ)
            champ_name = (
                champ_name.replace(" ", "")
                .replace("'", "")
                .replace(".", "")
                .replace(",", "")
                .lower()
            )

            if "nunu" in champ_name:
                champ_name = "nunu"

            champ_name = champ_name + ".png"
            path = os.path.join("cogs/data/champ_runes", champ_name)

            if os.path.isfile(path):
                await ctx.channel.send(f"Your runes{ctx.author} ")
                await ctx.channel.send(file=discord.File(path))
                print(f"I upload {champ_name}")
            else:
                await ctx.channel.send("There is no such champion in database!")
                print(f"There is no such a file as {champ_name}")
        else:
            await ctx.channel.send("Insert champion name!")

    @commands.command(name="fun_fact", help="Sends fun fuct about League of Legends")
    async def fun_fact(self, ctx):
        """Sends random fun fact from database about League of Legends to discord

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context:
        """

        texts = self.bot.db_connection.execute(
            f"""
        SELECT Text FROM FunFacts ORDER BY RANDOM() LIMIT 1;
        """
        )
        texts = list(texts)
        text = texts[0][0]

        await ctx.channel.send(f"```{text}```")


def setup(bot: commands.Bot):
    bot.add_cog(LOL(bot))
