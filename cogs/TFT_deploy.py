from discord.ext import commands
from cogs.class_creator.TFT import TFT
from cogs.comps import *


class TFTDeploy(TFT, name="TFT Composition Drawer"):
    pass


def setup(bot: commands.Bot):
    bot.add_cog(TFTDeploy(bot))
