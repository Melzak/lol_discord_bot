from abc import abstractmethod

from discord.ext import commands
from cogs.class_creator.TFT_helpers import RANKING, is_ranked_info
from cogs.class_creator.TFTDev import TFTDev


class TFT(TFTDev):
    @commands.command(
        name=f"draw",
        help="Enters the names of the players after the spaces, and we'll randomly choose what to play! :)",
        description="?draw [name1 name2 name3 ...], Command will randomly pick for each name one composition to play.",
    )
    async def draw_comps(self, ctx: commands.Context, *players: str) -> None:
        """Draws compositions for list of players

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param players: Tuple of players names to play
        :type players: Tuple[str]
        """

        if not players:
            await ctx.channel.send("Insert names!!!")
        elif len(players) > 8:
            await ctx.channel.send("Inserted too many player(max 8 players)!!!")
        else:
            await ctx.channel.send(f"You're in {RANKING[self.is_ranked]} mode!")

            if self.is_ranked is True:
                compositions = self.bot.db_connection.execute(
                    f"""
                SELECT 
                    CompositionName 
                FROM 
                    Compositions 
                WHERE 
                    (IsRanked = {True} AND IsBanned = {False})
                ORDER BY
                    RANDOM()
                LIMIT
                    {len(players)}
                """
                )
            else:
                compositions = self.bot.db_connection.execute(
                    f"""
                SELECT 
                    CompositionName
                FROM 
                    Compositions 
                WHERE 
                    IsBanned = {False}
                ORDER BY
                    RANDOM()
                LIMIT
                    {len(players)}
                """
                )
            compositions = list(comp[0] for comp in compositions)

            for player, comp in zip(players, compositions):
                if comp == "Doubled":
                    msg = f"""{player}: {" + ".join(self.doubled())}"""
                else:
                    msg = f"{player}: {comp}"
                await ctx.channel.send(msg, tts=self.tts)

    @commands.command(name=f"tts_switcher", help="Turn off or turn on TTS")
    async def tts_switcher(
        self, ctx: commands.Context, logic_value: bool = None
    ) -> None:
        """Changes logic value of self.tts

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param logic_value: Optional logic value to input
        :type logic_value: bool
        """

        self.tts = not self.tts if logic_value is None else logic_value

        if self.tts:
            ctx.channel.send("TTS is turned on!")
        else:
            ctx.channel.send("TTS is turned off!")

    @commands.command(name=f"available", help="Shows you all available comp to draw!")
    async def available(self, ctx) -> None:
        """Shows all compositions from database where IsBanned value is False

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        """

        if self.is_ranked is True:
            available_compositions = self.bot.db_connection.execute(
                f"""
                SELECT 
                    id, CompositionName 
                FROM 
                    Compositions 
                WHERE 
                    (IsRanked = {True} AND IsBanned = {False});
                """
            )
        else:
            available_compositions = self.bot.db_connection.execute(
                f"""
                SELECT 
                    id, CompositionName 
                FROM 
                    Compositions 
                WHERE 
                    IsBanned = {False};
                """
            )
        available_compositions = list(available_compositions)

        if available_compositions:
            await ctx.channel.send(
                "\n".join([f"{i}:{comp}" for i, comp in available_compositions])
            )
        else:
            await ctx.channel.send(f"There is no available composition to draw")

    @commands.command(
        name="switch_mode", help="Switches from ranked mode to fun and vice versa"
    )
    async def mode_switcher(self, ctx, logic_value: bool = None) -> None:
        """Changes logic value of self.is_ranked

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param logic_value: Optional logic value to input
        :type logic_value: bool
        """

        if logic_value is None:
            self.is_ranked = not self.is_ranked
            await ctx.channel.send(f"You're in {RANKING[self.is_ranked]} mode now!")
        elif type(logic_value) is bool:
            self.is_ranked = logic_value
            await ctx.channel.send(f"You're in {RANKING[self.is_ranked]} mode now!")
        else:
            await ctx.channel.send("You inserted non-boolean type!")

    @commands.command(name=f"banned", help="Show you all banned comps")
    async def banned(self, ctx):
        """Shows all compositions from database where IsBanned value is True

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        """

        banned_compositions = self.bot.db_connection.execute(
            f"""
        SELECT id, CompositionName FROM Compositions WHERE IsBanned = {True}
        """
        )
        banned_compositions = list(banned_compositions)

        if banned_compositions:
            await ctx.channel.send(
                "\n".join(
                    [f"{i}:{banned_comp}" for i, banned_comp in banned_compositions]
                )
            )
        else:
            await ctx.channel.send(f"There is no banned composition!")

    @commands.command(
        name=f"ban", help="Bans you the selected composition after the index"
    )
    async def ban(self, ctx, idx: int) -> None:
        """Chceck if composition with id == idx and IsBanned == False exists
        if it exists then sets column IsBanned to True

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param idx: Index of composition to ban
        :type idx: int
        """

        compositions = self.bot.db_connection.execute(
            f"""
            SELECT CompositionName FROM Compositions WHERE (id = {idx} AND IsBanned = {False})
        """
        )
        compositions = list(comp[0] for comp in compositions)

        if compositions:
            self.bot.db_connection.execute(
                f"""
                UPDATE Compositions SET IsBanned = {True} WHERE id = {idx}
            """
            )
            await ctx.channel.send(f"Successfully banned {compositions[0]}!!!")
        else:
            await ctx.channel.send(
                f"Composition with this index is banned or there is no composition with this id!!!"
            )

    @commands.command(
        name=f"unban", help="Unbans you the selected composition after the index"
    )
    async def unban(self, ctx, idx: int) -> None:
        """Chceck if composition with id == idx and IsBanned == True exists
        if it exists then sets column IsBanned to False

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param idx: Index of composition to unban
        """

        compositions = self.bot.db_connection.execute(
            f"""
            SELECT CompositionName FROM Compositions WHERE (id = {idx} AND IsBanned = {True})
        """
        )
        compositions = list(comp[0] for comp in compositions)

        if compositions:
            self.bot.db_connection.execute(
                f"""
                UPDATE Compositions SET IsBanned = {False} WHERE id = {idx}
            """
            )
            await ctx.channel.send(f"Successfully unbanned {compositions[0]}!!!")
        else:
            await ctx.channel.send(f"Composition with this index is not banned!!!")

    @commands.command(name="reset", help="Resets compositions lists to basic form")
    async def reset(self, ctx) -> None:
        """Removes all compositions where column IsTemporary == True from database

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        """

        self.bot.db_connection.execute(
            f"""
        DELETE FROM Compositions WHERE IsTemporary = {True}
        """
        )
        self.bot.db_connection.execute(
            f"""
        UPDATE Compositions SET IsBanned = {False}
        """
        )

        await ctx.channel.send(f"Successfully reset compositions to basic form!!!")

    def doubled(self) -> tuple:
        """Loads two compositions from data base for player

        :return: Returns string with two team comps
        :rtype: tuple
        """

        compositions = self.bot.db_connection.execute(
            f"""
        SELECT 
            CompositionName 
        FROM 
            Compositions 
        WHERE 
            (IsRanked = {True} AND IsBanned = {False})
        ORDER BY
            RANDOM()
        LIMIT 2;
        """
        )

        compositions = tuple(comp[0] for comp in compositions)
        return compositions
