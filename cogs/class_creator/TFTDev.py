from time import time

from discord.ext import commands


class TFTDev(commands.Cog):
    def __init__(self, bot, tts=False):
        self.bot = bot
        self.tts = tts
        self.is_ranked = True

    @commands.has_role("Devs")
    @commands.command(name=f"add", help="Adds comps to available list")
    async def add(self, ctx: commands.Context, comp_name: str) -> None:
        """Method will insert new row to database if It doesn't already exist

        Every new composition in database has set:
         IsTemporary column to True
         IsRanked column to False

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param comp_name: Composition name to insert into database
        :type comp_name: str
        """

        comp_name = " ".join(comp_name)

        if not comp_name:
            await ctx.channel.send(f"Insert composition name to add!!!")

        compositions = self.bot.db_connection.execute(
            f"""
            SELECT * FROM Compositions WHERE CompositionName = '{comp_name}'
            """
        )
        compositions = list(comp[0] for comp in compositions)

        if compositions:
            await ctx.channel.send(f"There is already such a build!!!")
        else:
            self.bot.db_connection.execute(
                f"""
            INSERT INTO Compositions (CompositionName, IsRanked, IsBanned, IsTemporary, LastUpdate)
            VALUES ('{comp_name}', {False}, {False}, {True} ,{int(time())} );
            """
            )
            await ctx.channel.send(f"""Successfully added {comp_name}!!!""")

    @commands.has_role("Devs")
    @commands.command(
        name=f"delete", help="Deletes you the selected composition after the index "
    )
    async def delete(self, ctx: commands.Context, idx: int) -> None:
        """Checks if composition with id == idx and IsTemporary == True exists
        if composition exists command will delete composition after index from database

        :param ctx: Context manager is used to communicate with discord API
        :type ctx: commands.Context
        :param idx: Index of composition to delete from database
        :type idx: int
        """

        if type(idx) is not int:
            await ctx.channel.send(f"Insert INDEX of comp to delete!!!")
        else:
            await ctx.channel.send(f"There is no such an index!!!")

        compositions = self.bot.db_connection.execute(
            f"""
            SELECT CompositionName FROM Compositions WHERE (id = {idx} AND IsTemporary = {True});
        """
        )
        compositions = list(comp[0] for comp in compositions)

        if compositions:
            self.bot.db_connection.execute(
                f"""
                DELETE FROM Compositions WHERE id = {idx}
            """
            )
            await ctx.channel.send(
                f"Successfully deleted composition: {compositions[0]}"
            )
        else:
            await ctx.channel.send(f"There is no such an index to delete!!!")
