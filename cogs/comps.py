from copy import copy

basic_builds = [
    "Abomination",
    "Coven",
    "Dawnbringer",
    "Draconic",
    "Dragonslayer",
    "Forgotten",
    "Hellion",
    "Ironclad",
    "Nightbringer",
    "Redeemed",
    "Revenant",
    "Verdant",
    "Assasin",
    "Brawler",
    "Cavalier",
    "Invoker",
    "Knight",
    "Legionnaire",
    "Mystic",
    "Ranger",
    "Renewer",
    "Skirmisher",
    "Spellweaver",
    "Whatever",
    "As many as possible",
    "One champ",
    "Team for Gold",
    "Team for 5",
    "Doubled",
]

ranked_builds = copy(basic_builds[:-6])
fun_builds = copy(basic_builds)

banned_ranked = []
banned_fun = []

UNRANKED_NUM = 6
