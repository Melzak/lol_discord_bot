from dsc_client.client import MyBot

bot = MyBot(command_prefix="?")

bot.load_extension("cogs.TFT_deploy")
bot.load_extension("cogs.LOL")

bot.run(bot.TOKEN)
